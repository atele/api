#! /usr/bin/env python
import os

from flask_assets import ManageAssets

from model_package import models

from plant import setup_app, initialize_api

app = setup_app('api', 'DevelopmentConfig', models.db)
manager = app.manager
manager.add_command('assets', ManageAssets)

# Allow the manager access config files on the fly
manager.add_option('-c', '--config', dest='config_obj', default='settings.DevelopmentConfig', required=False)


with app.app_context():
    @manager.command
    def run():

        from resources import account, base, api, authentication, equipment, payment
        initialize_api(app, app.api)

        port = int(os.environ.get('PORT', 5640))
        app.run(host='0.0.0.0', port=port)


    if __name__ == "__main__":
        manager.run()
