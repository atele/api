import os

from model_package import models

from plant import setup_app, initialize_api

app = setup_app('customer_app', 'ProductionConfig', models.db)
db = app.db

with app.app_context():

    from resources import account, base, api, authentication, equipment, payment
    initialize_api(app, app.api)

    if __name__ == '__main__':
        app.run(use_reloader=False)
