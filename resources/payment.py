from flask import current_app as app
from flask_restful import fields, abort

from services_package import payment, basic, account
from resources import ModelField, ModelListField, authentication
from model_package import forms


class CustomerBillingRecordResource(authentication.ClientBaseResource):
    """
    Reading resource
    """
    service_class = payment.BillingRecordService
    resource_name = 'customer_billing_records'
    validation_form = forms.BillingRecordForm
    resource_fields = {
        'billing_id': fields.Integer,
        'billing': ModelField,
        'customer_id': fields.Integer,
        'customer': ModelField,
        'device_id': fields.Integer,
        'device': ModelField,
        'mesh_id': fields.Integer,
        'mesh': ModelField,
        'client_id': fields.Integer,
        'client': ModelField
    }


class CustomerBillingResource(authentication.ClientBaseResource):
    """
    DeviceType resource
    """
    service_class = payment.BillingService
    resource_name = 'customer_billings'
    validation_form = forms.BillingForm
    resource_fields = {
        'start_date': fields.DateTime(dt_format='rfc822'),
        'end_date': fields.DateTime(dt_format='rfc822'),
        'amount': fields.Float,
        'customer_id': fields.Integer,
        'customer': ModelField,
        'records': ModelListField
    }


class BillingRecordResource(authentication.ClientBaseResource):
    """
    Device resource
    """
    service_class = payment.BillingRecordService
    resource_name = 'billing_records'
    validation_form = forms.ClientBillingRecordForm
    resource_fields = {
        'billing_id': fields.Integer,
        'billing': ModelField,
        'device_id': fields.Integer,
        'device': ModelField,
        'mesh_id': fields.Integer,
        'mesh': ModelField,
        'client_id': fields.Integer,
        'client': ModelField,
        'subscription_id': fields.Integer,
        'subscription': ModelField
    }


class BillingResource(authentication.ClientBaseResource):
    """
    Mesh resource
    """
    service_class = payment.BillingService
    resource_name = 'billings'
    validation_form = forms.ClientBillingForm
    resource_fields = {
        'subscription_id': fields.Integer,
        'subscription': ModelField,
        'records': ModelListField,
        'start_date': fields.DateTime(dt_format='rfc822'),
        'end_date': fields.DateTime(dt_format='rfc822'),
        'amount': fields.Float
    }


# class CardResource(BaseResource):
#     """
#     Installation resource
#     """
#     service_class = payment.CardService
#     resource_name = 'cards'
#     validation_form = forms.CardForm
#     resource_fields = {
#         'client_id': fields.Integer,
#         'client': ModelField,
#         'wallet_id': fields.Integer,
#         'wallet': ModelField,
#         'address_id': fields.Integer,
#         'address': ModelField,
#         'card_number': fields.String,
#         'unique_key': fields.String,
#         'customer_id': fields.Integer,
#         'customer': ModelField
#     }


class InvoiceResource(authentication.ClientBaseResource):
    """
    Installation resource
    """
    service_class = payment.InvoiceService
    resource_name = 'invoices'
    validation_form = forms.InvoiceForm
    resource_fields = {
        'client_id': fields.Integer,
        'client': ModelField,
        'customer_id': fields.Integer,
        'customer': ModelField
    }

    def post(self, obj_id=None, **kwargs):
        abort(405)


class SubscriptionResource(authentication.ClientBaseResource):
    """
    Subscription resource
    """
    service_class = payment.SubscriptionService
    resource_name = 'subscriptions'
    validation_form = forms.SubscriptionForm
    resource_fields = {
        'client_id': fields.Integer,
        'client': ModelField,
        'service_id': fields.Integer,
        'service': ModelField,
        'billings': ModelListField
    }

    def adjust_form_fields(self, form):
        form.service_id.choices = [(c.id, c.name) for c in basic.CityService.all()]
        form.client_id.choices = [(c.id, c.name) for c in account.ClientService.all()]

        return form


app.api.add_resource(BillingRecordResource, '/billing_records', '/billing_records/<int:obj_id>')
app.api.add_resource(BillingResource, '/billings', '/billings/<int:obj_id>')
app.api.add_resource(CustomerBillingRecordResource, '/customer_billing_records', '/customer_billing_records/<int:obj_id>')
app.api.add_resource(CustomerBillingResource, '/customer_billings', '/customer_billings/<int:obj_id>')
app.api.add_resource(SubscriptionResource, '/subscriptions', '/subscriptions/<int:obj_id>')
app.api.add_resource(InvoiceResource, '/invoices', '/invoices/<int:obj_id>')
# app.api.add_resource(CardResource, '/cards', '/cards/<int:obj_id>')
