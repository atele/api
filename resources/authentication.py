from flask import current_app, request, g
from flask_httpauth import HTTPBasicAuth, HTTPTokenAuth
from model_package import models, forms

from utilities.exceptions import ValidationFailed
from resources import BaseResource, ApiResource
from services_package import authentication

admin_auth = HTTPBasicAuth()
auth = HTTPTokenAuth('Bearer')
client_auth = HTTPTokenAuth('Bearer')
# admin_auth = HTTPTokenAuth(scheme='Token')


@auth.verify_token
def verify_token(token):
    """
    verify user token
    :param token:
    :return:
    """
    data = authentication.decode_auth_token(token)
    if isinstance(data, dict) and 'sub' in data:
        user = models.User.query.get(data.get('sub'))
        if user:
            g.user = user
            return True
    return False


@client_auth.verify_token
def verify_client(token):
    """
    verify client token
    :param token:
    :return:
    """
    data = authentication.decode_auth_token(token)
    if isinstance(data, dict) and 'sub' in data:
        user = models.User.query.get(data.get('sub'))
        if user and hasattr(user, 'client'):
            g.user = user
            g.client = user.client
            return True
    return False


@admin_auth.verify_password
def verify_admin(username, password):
    admin = models.Admin.query.filter_by(username=username).first()

    if not admin or not admin.check_password(password):
        return False
    g.admin = admin
    return True


class LoginResource(ApiResource):
    """
    Login api
    """

    resource_name = "login"
    method_decorators = []

    def post(self):

        _data = request.data or {}
        _data.update(request.form)

        form = forms.LoginForm(**_data)
        form.meta.csrf = False
        if form.validate():
            data = form.data

            user = authentication.authenticate(**data)

            if user is None:
                return self.prepare_response(401, {"status": "failure",
                                                   "message": "The provided user credentials are invalid."})
            else:
                if user.is_verified is False:
                    return self.prepare_response(401,
                                                 {"status": "failure", "message": "User Verification not complete."})

                return self.prepare_response(200, {"user_id": user.id,
                                                   "auth_token": authentication.encode_auth_token(user.id)})

        else:
            raise ValidationFailed(data=self.prepare_errors(form.errors))


class MixinResource(BaseResource):
    """
        user authentication protection on api resources
    """

    @auth.login_required
    def get(self, obj_id=None):
        return super(MixinResource, self).get(obj_id)

    @client_auth.login_required
    def post(self, obj_id=None, **kwargs):
        return super(MixinResource, self).post(obj_id, **kwargs)

    @client_auth.login_required
    def put(self, obj_id=None, resource_name=None, obj_ids=None, **kwargs):
        return super(MixinResource, self).post(obj_id, resource_name, obj_ids, **kwargs)

    @client_auth.login_required
    def delete(self, obj_id=None, **kwargs):
        return super(MixinResource, self).delete(obj_id)


class ClientBaseResource(MixinResource):
    """
    client authentication protection on api resources
    """

    @client_auth.login_required
    def get(self, obj_id=None):
        return super(MixinResource, self).get(obj_id)

    def query(self):
        return self.service_class.query.filter_by(client_id=self.get_client_id())


class AdminBaseResource(BaseResource):
    """
    admin authentication protection on api resources
    """

    @admin_auth.login_required
    def get(self, obj_id=None):
        return super(AdminBaseResource, self).get(obj_id)


class PublicBaseResource(BaseResource):
    """
    admin authentication protection on api resources
    """

    def get(self, obj_id=None):
        return super(PublicBaseResource, self).get(obj_id)

    @admin_auth.login_required
    def post(self, obj_id=None, **kwargs):
        return super(PublicBaseResource).post(obj_id, **kwargs)

    @admin_auth.login_required
    def put(self, obj_id=None, **kwargs):
        return super(PublicBaseResource).put(obj_id, **kwargs)

    @admin_auth.login_required
    def delete(self, obj_id):
        return super(PublicBaseResource, self).delete(obj_id)


current_app.api.add_resource(LoginResource, '/login')
