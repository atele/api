from flask import current_app as app
from flask_restful import fields, reqparse, abort

from model_package import models, forms
from services_package import account, basic, equipment
from resources import BaseResource, ModelField, ModelListField, ObjectNotFoundException
from resources.authentication import ClientBaseResource


class ClientResource(BaseResource):
    """
    Client resource
    """
    service_class = account.ClientService
    resource_name = 'client'
    validation_form = forms.ClientForm
    resource_fields = {
        'name': fields.String,
        'address_id': fields.Integer,
        'email': fields.String,
        'url': fields.String,
        'address': ModelField,
        'customers': ModelListField,
        'installations': ModelListField,
        'devices': ModelListField
    }

    def adjust_form_fields(self, form):
        form.city_id.choices = [(c.id, c.name) for c in basic.CityService.all()]
        form.state_id.choices = [(c.id, c.name) for c in basic.StateService.all()]
        form.country_id.choices = [(c.id, c.name) for c in basic.CountryService.all()]

        return form


class UserResource(ClientBaseResource):
    """
    User api resource
    """
    service_class = account.UserService
    resource_name = 'user'
    validation_form = forms.UserForm
    resource_fields = {
        'address_id': fields.Integer,
        'address': ModelField,
        'email': fields.String,
        'is_active': fields.Boolean,
        'is_verified': fields.Boolean,
        'is_staff': fields.Boolean,
        'is_admin': fields.Boolean,
        'is_super_admin': fields.Boolean,
        'is_demo_account': fields.Boolean,
        'client_id': fields.Integer,
        'client': ModelField
    }

    def adjust_form_fields(self, form):
        form.city_id.choices = [(c.id, c.name) for c in basic.CityService.all()]
        form.state_id.choices = [(c.id, c.name) for c in basic.StateService.all()]
        form.country_id.choices = [(c.id, c.name) for c in basic.CountryService.all()]

        return form

    def adjust_form_data(self, data):
        data['client_id'] = self.get_client_id()
        return data


class CustomerResource(ClientBaseResource):
    """
    User api resource
    """
    service_class = account.CustomerService
    validation_form = forms.CustomerForm
    resource_name = 'customer'
    resource_fields = {
        'address_id': fields.Integer,
        'address': ModelField,
        # 'client_id': fields.Integer,
        # 'client': ModelField(exclude=['city', 'state', 'installations', 'subscriptions', 'settings', 'devices',
        #                               'country', 'wallet', 'bank_accounts', 'customers', 'cards', 'invoices',
        #                               'reports']),
        # 'cards': ModelListField,
        'devices': ModelListField
    }

    @staticmethod
    def filter_parser():
        """
        updated filter parser argument
        :return:
        """
        parser = reqparse.RequestParser()
        parser.add_argument('client_id', type=int, location='args')
        parser.add_argument('station_id', type=int, location='args')
        return parser.parse_args()

    def adjust_form_fields(self, form):
        form.city_id.choices = [(c.id, c.name) for c in basic.CityService.all()]
        form.state_id.choices = [(c.id, c.name) for c in basic.StateService.all()]
        form.country_id.choices = [(c.id, c.name) for c in basic.CountryService.all()]
        form.product_id.choices = [(c.id, c.name) for c in basic.ProductService.all()]
        form.network_id.choices = [(c.id, c.code) for c in equipment.NetworkService.all()]
        form.station_id.choices = [(c.id, c.name) for c in equipment.StationService.all()]

        return form

    def query(self):
        client_id = self.filter_parser().pop('client_id', None)
        if client_id:
            try:
                return self.service_class.query.join(models.client_customers, account.ClientService.model_class).\
                    filter(account.ClientService.model_class.id==client_id)
            except ObjectNotFoundException:
                abort(404)
        return self.service_class.query
        # return self.service_class.query.join(account.ClientService.model_class).filter_by(
        #     id=client_id)


app.api.add_resource(ClientResource, '/clients', '/clients/<int:obj_id>')
app.api.add_resource(UserResource, '/users', '/users/<int:obj_id>')
app.api.add_resource(CustomerResource, '/customers', '/customers/<int:obj_id>')
