from logging import Formatter, ERROR, handlers

from flask import Flask
from flask_restful import Api
from flask_script import Manager
from celery import Celery
from raven.contrib.flask import Sentry
import htmlmin

from model_package import models

import settings


def render_domain_template(template_name, **kwargs):
    from flask import render_template

    _html = render_template(template_name, **kwargs)

    return htmlmin.minify(_html, remove_empty_space=True, remove_comments=True)


def setup_app(name, config_class, db):
    module = getattr(settings, config_class)
    app = Flask('api')
    app.config.from_object(module)

    # api resources
    app.api = Api(app, prefix='/v1')
    db.init_app(app)
    app.db = db

    # flask script manager
    app.manager = Manager(app)

    # inject celery into the app
    app.celery = models.app.celery

    file_handler = handlers.RotatingFileHandler("/var/log/api/%s.log" % app.config.get("LOGFILE_NAME", name),
                                                maxBytes=500 * 1024)
    file_handler.setLevel(ERROR)
    file_handler.setFormatter(Formatter(
        '%(asctime)s %(levelname)s: %(message)s '
        '[in %(pathname)s:%(lineno)d]'
    ))
    app.logger.addHandler(file_handler)

    # configure sentry
    if not app.config.get("DEBUG", True):
        sentry = Sentry(app)
        app.sentry = sentry

    return app


def initialize_api(app, api):
    """ Register all resources for the API """
    api.init_app(app=app)  # Initialize api first
    _resources = getattr(app, "api_registry", None)
    if _resources and isinstance(_resources, (list, tuple,)):
        for cls, args, kwargs in _resources:
            api.add_resource(cls, *args, **kwargs)
